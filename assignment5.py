#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
{'title': 'Algorithm Design', 'id':'2'}, \
{'title': 'Python', 'id':'3'}]

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)

@app.route('/book/JSON/')
def bookJSON():
	return json.dumps(books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        thisBook={}
        newID = -1
        if len(books) == int(books[len(books)-1]['id']):
            newID = len(books)+1
        else:
            for i in range(len(books)):
                if(books[i]['id'] != str(i+1)):
                    newID = i+1
                    break
        newTitle = request.form['name']
        thisBook['title'] = newTitle
        thisBook['id'] = str(newID)
        books.insert(newID-1,thisBook)
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')
	
@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    for book in books:
        if book['id'] == str(book_id):
            thisBook = book
    if request.method == 'POST':
        newTitle = request.form['name']
        thisBook['title'] = newTitle
        
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', title=thisBook['title'], book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    for book in books:
        if book['id'] == str(book_id):
            thisBook = book
    if request.method == 'POST':
        for book in books:
            if book['id'] == str(book_id):
                books.remove(book)
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html',title=thisBook['title'], book_id = book_id)
	
if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 5000)
	

